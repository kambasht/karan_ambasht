"""Main script for generating output.csv."""
# Import required modules
import pandas as pd
import numpy as np

# Read in the raw pitch data
df_raw =pd.read_csv(".//data//raw//pitchdata.csv")

# read in the combinations file
df_combinations = pd.read_csv(".//data//reference//combinations.txt")

# This list will keep a list of all the dataframes that we will be making for each line of combinations.txt
# This list will be then used to concatenate all the dataframes together
df_list =[]


# The for loop is for going through each line of the combinations.txt file
for i in range(0,len(df_combinations)):
    # read in the values from combinations.txt
    stat = df_combinations.iloc[i]["Stat"]
    subject = df_combinations.iloc[i]["Subject"]
    handedness = df_combinations.iloc[i]["Split"]
    
    # we separate out the left/right handed and Pitcher or Hitter data
    dummy, useful = handedness.split()
    LorRhanded = useful[0]
    PorH = useful[2:]

    # filter based on pitcher/hitter and L/R handed
    if LorRhanded=='L' and PorH=='P':
        df_filtered = df_raw[df_raw.PitcherSide==LorRhanded]
    elif LorRhanded=='R' and PorH=='P':
        df_filtered = df_raw[df_raw.PitcherSide==LorRhanded]
    elif LorRhanded=='L' and PorH=='H':
        df_filtered = df_raw[df_raw.HitterSide==LorRhanded]
    elif LorRhanded=='R' and PorH=='H':
        df_filtered = df_raw[df_raw.HitterSide==LorRhanded]
    
    # As per the requirement group by with the subject and store the sum in another dataframe
    df_filtered2 = df_filtered.groupby(subject).sum()
    # filter out the rows with PA >=25 as per requirement
    df_filtered1 = df_filtered2[df_filtered2.PA >= 25]
    
    # This is the dataframe that will be created after every line of combinations.txt has produced a dataframe
    df_final = pd.DataFrame(index=np.arange(0,len(df_filtered1)),columns=['SubjectId','Stat','Split','Subject','Value'])   
    
    # The for loop will now go through all the group by rows and calculate the required AVG,OBP,SLG or OPS
    for i in range(0,len(df_filtered1)):
        
        # The three columns subjectid,split and subject remain as following for each row
        df_final.iloc[i]['SubjectId']= df_filtered1.index[i]
        df_final.iloc[i]['Split']= handedness
        df_final.iloc[i]['Subject']= subject
        
        # perform calculations based on "stat" value
        if stat=='AVG':    
            df_final.iloc[i]['Stat'] ='AVG'
            df_final.iloc[i]['Value']=round(df_filtered1.iloc[i]['H']/df_filtered1.iloc[i]['AB'],3)

        elif stat=='OBP':
            df_final.iloc[i]['Stat']='OBP'
            df_final.iloc[i]['Value']=round((df_filtered1.iloc[i]['H'] + df_filtered1.iloc[i]['BB'] + df_filtered1.iloc[i]['HBP'])/(df_filtered1.iloc[i]['AB']+df_filtered1.iloc[i]['BB']+df_filtered1.iloc[i]['HBP']+df_filtered1.iloc[i]['SF']),3)

        elif stat=='SLG':
            df_final.iloc[i]['Stat']='SLG'
            df_final.iloc[i]['Value']=round(df_filtered1.iloc[i]['TB']/df_filtered1.iloc[i]['AB'],3)

        elif stat=='OPS':
            df_final.iloc[i]['Stat']='OPS'
            df_final.iloc[i]['Value']=round(((df_filtered1.iloc[i]['AB'])*(df_filtered1.iloc[i]['H']+df_filtered1.iloc[i]['BB']+df_filtered1.iloc[i]['HBP'])+ (df_filtered1.iloc[i]['TB'])*(df_filtered1.iloc[i]['AB']+df_filtered1.iloc[i]['BB']+df_filtered1.iloc[i]['SF']+df_filtered1.iloc[i]['HBP']))/(df_filtered1.iloc[i]['AB']*(df_filtered1.iloc[i]['AB']+df_filtered1.iloc[i]['BB']+df_filtered1.iloc[i]['SF']+df_filtered1.iloc[i]['HBP'])),3)
    # append the data to our list
    df_list.append(df_final)
    

# df_univ is the universal dataframe which will be our final frame
df_univ = pd.concat(df_list,ignore_index=True)

# sort values with respect to first three columns
df_univ_sorted=df_univ.sort_values(['SubjectId','Stat','Split','Subject'])
df_univ_sorted = df_univ_sorted.reset_index(drop=True)
# save the data in the processed folder as "output.csv"
df_univ_sorted.to_csv(".//data//processed//output.csv",index=False)
