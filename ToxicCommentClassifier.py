
# Motivation
# 
# The last decade has witnessed a surge of research on cyberbullying, a type of bullying that
# occurs through the use of electronic communication technologies. Cyberbullying can take place
# on social media sites such as Facebook, Myspace, and Twitter. "By 2008, 93% of young people
# between the ages of 12 and 17 were online. In fact, youth spend more time with media than
# any single other activity besides sleeping."
# 
# The big Internet companies, like Facebook,Twitter and Google, struggle to effectively facilitate
# conversations and prevent pervasive cyber bullying on the Internet. One area of focus is the
# ability to analyze and predict negative online behaviors, like toxic comments (i.e. comments
# that are rude, disrespectful or otherwise likely to make someone leave a discussion).
# We are very interested in this domain and hope to make our contributions to prevent cyber
# bullying. Our goal is to build a multi-headed model that’s capable of detecting different types
# of toxicity such as threats, obscenity, insults, and identity-based hate which will help social
# platforms in detecting them in a preemptive manner before they get reported.
# 

# In[1]:


#import required libraries
import numpy as np
import pandas as pd
import os
import seaborn as sns
import matplotlib.pyplot as plt
import string
import textblob

from sklearn.linear_model import LogisticRegression
from scipy.sparse import hstack
from sklearn.model_selection import train_test_split
from sklearn import metrics, svm
from sklearn.neighbors import KNeighborsRegressor
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.metrics import confusion_matrix, roc_auc_score

import nltk
nltk.download('averaged_perceptron_tagger')
nltk.download('vader_lexicon')


# In[2]:


pd.options.mode.chained_assignment = None  # for taking care of warning messages

# column names
class_names = ['toxic', 'severe_toxic', 'obscene', 'threat', 'insult', 'identity_hate']

# read a sample set of train and test data
raw_data = pd.read_csv('./train/train.csv').fillna(' ')
sample = raw_data.sample(frac=0.1, replace=False)

# random split the raw data, 80% as the train dataset and 20% as the test data
train, test = train_test_split(sample, test_size=0.2)

train_text = train['comment_text']
test_text = test['comment_text']

all_text = pd.concat([train_text, test_text])


# In[3]:


raw_data.head(5)


# In[4]:


# exploration of our data set
train.info()


# Above information tells us that there are no missing values in the frame
# Also tells us that all the label columns are stored as int64 hence we are good to do operations on the columns

# Lets now see the distribution of the labels i.e. how many of each type of toxicity is contained in the training set

# In[5]:


train_markers= train[['toxic', 'severe_toxic', 'obscene', 'threat', 'insult','identity_hate']]
train_markers.head()
train_markers.columns.tolist()
train_markers.sum().tolist()


# In[6]:


# visualize the distribution

fontsize= 13
plt.rcParams['figure.figsize']=(12,8)
plt.rc('xtick', labelsize=fontsize) 
plt.rc('ytick', labelsize=fontsize) 
sns.barplot(train_markers.columns.tolist(),train_markers.sum().tolist())

plt.xlabel("Comment Type",size=fontsize)
plt.ylabel("Number of occurences",size=fontsize)

plt.show()


# We can see how many comments are completely clean, i.e. all columns have value 0

# In[7]:


train[(train.sum(axis=1))==0].id.count()


# In[8]:


train[(train.sum(axis=1))==0].id.count() / train.id.count()


# Close to 90% of comments are clean in the data

# In[9]:


toxic = pd.DataFrame(train[(train["toxic"]==1) | (train["severe_toxic"]==1) ]['comment_text'])
# sample some of the toxic and severe toxic comments
toxic.head(10)


# # Lets build a wordcloud for the most frequent words being used in the threat comments

# In[10]:


#storing each categories of non clean comments in specific arrays
toxic = train[train.toxic==1]['comment_text'].values
severe_toxic = train[train.severe_toxic==1]['comment_text'].values
obscene = train[train.obscene==1]['comment_text'].values
threat = train[train.threat==1]['comment_text'].values
insult = train[train.insult==1]['comment_text'].values
identity_hate = train[train.identity_hate==1]['comment_text'].values
toxic_comments = [toxic, severe_toxic, obscene, threat, insult, identity_hate]


# In[11]:


from wordcloud import WordCloud, STOPWORDS

# Method to produce wordcloud for each type of comments
def plt_wordCloud(text):
    plt.figure(figsize=(15,13))
    wc = WordCloud(background_color="white", 
                   max_words=100,
                   stopwords=STOPWORDS, 
                   max_font_size= 50,
                   collocations=False, 
                   width=400, 
                   height=200)
    wc = wc.generate(" ".join(text))   # this collates all the data 
    plt.title("Wordcloud toxic Comments", fontsize=25)
    plt.imshow(wc.recolor(colormap= 'Set1' , random_state=1), alpha=0.98)
    plt.axis('off')
    plt.show()


# In[12]:


# lets build a word cloud for each type of toxicity, i.e toxic, sever_toxic, identity hate, insult, obscene, threat
for toxic_comment in toxic_comments:
    plt_wordCloud(toxic_comment)


# # Feature Engineering
# #### The next step is the feature engineering step. In this step, raw text data will be transformed into feature vectors and new features will be created using the existing dataset. We will implement the following different ideas in order to obtain relevant features from our dataset.
# 
# #### 2.1 TF-IDF Vectors as features
# 
#     Word level
#     N-Gram level
# #### 2.2 Text / NLP based features
# #### 2.3 Sentiment Analysis features

# We will now initiate a model for each of these classifiers and use them in the Data modelling section to predict our accuracy scores

# ## 2.1 TF-IDF Vectors as features
# TF-IDF score represents the relative importance of a term in the document and the entire corpus. TF-IDF score is composed by two terms: the first computes the normalized Term Frequency (TF), the second term is the Inverse Document Frequency (IDF), computed as the logarithm of the number of the documents in the corpus divided by the number of documents where the specific term appears.
# 
# TF(t) = (Number of times term t appears in a document) / (Total number of terms in the document)
# IDF(t) = log_e(Total number of documents / Number of documents with term t in it)
# 
# TF-IDF Vectors can be generated at different levels of input tokens (words, characters, n-grams)
# 
#     a. Word Level TF-IDF : Matrix representing tf-idf scores of every term in different documents
#     b. N-gram Level TF-IDF : N-grams are the combination of N terms together. This Matrix representing tf-idf scores of N-grams
#     c. Character Level TF-IDF : Matrix representing tf-idf scores of character level n-grams in the corpus

# In[13]:


get_ipython().run_cell_magic('time', '', "# Create a Tfidf vectorizer\nword_level_TFIDF = TfidfVectorizer(\n    sublinear_tf=True,\n    strip_accents='unicode',\n    analyzer='word',\n    token_pattern=r'\\w{1,}',\n    stop_words='english',\n    ngram_range=(1, 1))")


# In[14]:


get_ipython().run_cell_magic('time', '', '# transform the training and test data using tfidf vectorizer object\nword_level_TFIDF.fit(all_text)\ntrain_word_tfidf_features = word_level_TFIDF.transform(train_text)\ntest_word_tfidf_features = word_level_TFIDF.transform(test_text)')


# In[15]:


get_ipython().run_cell_magic('time', '', "# Create a ngram level Tfidf vectorizer\nNgram_level_TFIDF = TfidfVectorizer(\n    sublinear_tf=True,\n    strip_accents='unicode',\n    analyzer='word',\n    token_pattern=r'\\w{1,}',\n    ngram_range=(3, 6))")


# In[16]:


get_ipython().run_cell_magic('time', '', '# transform the training and test data using tfidf vectorizer(ngram 3, 6) object\nNgram_level_TFIDF.fit(all_text)\ntrain_ngram_tfidf_features = Ngram_level_TFIDF.transform(train_text)\ntest_ngram_tfidf_features = Ngram_level_TFIDF.transform(test_text)')


# ## 2.2 Text / NLP based features
# A number of extra text based features can also be created which sometimes are helpful for improving text classification models. Some examples are:
# 
#     Word Count of the documents – total number of words in the documents
#     Character Count of the documents – total number of characters in the documents
#     Average Word Density of the documents – average length of the words used in the documents
#     Puncutation Count in the Complete Essay – total number of punctuation marks in the documents
#     Upper Case Count in the Complete Essay – total number of upper count words in the documents
#     Frequency distribution of Part of Speech Tags:
#         Noun Count
#         Verb Count
#         Adjective Count
#         Adverb Count
#         Pronoun Count
#         
# These features are highly experimental ones and should be used according to the problem statement only.

# In[17]:


get_ipython().run_cell_magic('time', '', 'train[\'char_count\'] = train[\'comment_text\'].apply(len)\ntrain[\'word_count\'] = train[\'comment_text\'].apply(lambda x: len(x.split()))\ntrain[\'word_density\'] = train[\'char_count\'] / (train[\'word_count\']+1)\ntrain[\'punctuation_count\'] = train[\'comment_text\'].apply(lambda x: len("".join(_ for _ in x if _ in string.punctuation))) \ntrain[\'upper_case_word_count\'] = train[\'comment_text\'].apply(lambda x: len([wrd for wrd in x.split() if wrd.isupper()]))\n#to keep the features value be normalized, between 0 and 1\ntrain[\'upper_case_density\'] = train[\'upper_case_word_count\'] / (train[\'word_count\']+1)\ntrain[\'punctuation_density\'] = train[\'punctuation_count\'] / (train[\'word_count\']+1)')


# In[18]:


train.head()


# In[19]:


get_ipython().run_cell_magic('time', '', '#get NLP features on the test data\ntest[\'char_count\'] = test[\'comment_text\'].apply(len)\ntest[\'word_count\'] = test[\'comment_text\'].apply(lambda x: len(x.split()))\ntest[\'word_density\'] = test[\'char_count\'] / (test[\'word_count\']+1)\ntest[\'punctuation_count\'] = test[\'comment_text\'].apply(lambda x: len("".join(_ for _ in x if _ in string.punctuation))) \ntest[\'upper_case_word_count\'] = test[\'comment_text\'].apply(lambda x: len([wrd for wrd in x.split() if wrd.isupper()]))\ntest[\'upper_case_density\'] = test[\'upper_case_word_count\'] / (test[\'word_count\']+1)\ntest[\'punctuation_density\'] = test[\'punctuation_count\'] / (test[\'word_count\']+1)')


# In[20]:


# dataframe with new columns added with NLP features
test.head()


# In[21]:


pos_family = {
    'noun' : ['NN','NNS','NNP','NNPS'],
    'pron' : ['PRP','PRP$','WP','WP$'],
    'verb' : ['VB','VBD','VBG','VBN','VBP','VBZ'],
    'adj' :  ['JJ','JJR','JJS'],
    'adv' : ['RB','RBR','RBS','WRB']
}

# function to check and get the part of speech tag count of a words in a given sentence
def check_pos_tag(x, flag):
    cnt = 0
    try:
        wiki = textblob.TextBlob(x)
        for tup in wiki.tags:
            ppo = list(tup)[1]
            if ppo in pos_family[flag]:
                cnt += 1
    except:
        pass
    return cnt


# In[22]:


get_ipython().run_cell_magic('time', '', "\n#count the position tag for each comment\ntrain['noun_count'] = train['comment_text'].apply(lambda x: check_pos_tag(x, 'noun'))\ntrain['verb_count'] = train['comment_text'].apply(lambda x: check_pos_tag(x, 'verb'))\ntrain['adj_count'] = train['comment_text'].apply(lambda x: check_pos_tag(x, 'adj'))\ntrain['adv_count'] = train['comment_text'].apply(lambda x: check_pos_tag(x, 'adv'))\ntrain['pron_count'] = train['comment_text'].apply(lambda x: check_pos_tag(x, 'pron'))\n\n#calculate the density to keep normalized data\ntrain['noun_density'] = train['noun_count'] / (train['word_count']+1)\ntrain['verb_density'] = train['verb_count'] / (train['word_count']+1)\ntrain['adj_density'] = train['adj_count'] / (train['word_count']+1)\ntrain['adv_density'] = train['adv_count'] / (train['word_count']+1)\ntrain['pron_density'] = train['pron_count'] / (train['word_count']+1)")


# In[23]:


train.head()


# In[24]:


#get nlp featrues from train dataset
train_nlp_features = train.loc[:, ['word_density','upper_case_density','punctuation_density',
                                   'noun_density','verb_density','adj_density','adv_density',
                                   'pron_density']]


# In[25]:


#Check the feature values
train_nlp_features.values


# In[26]:


# dataframe with added columns of densities
train.head()


# In[27]:


get_ipython().run_line_magic('time', '')
#calculate nlp features on test dataset
test['noun_count'] = test['comment_text'].apply(lambda x: check_pos_tag(x, 'noun'))
test['verb_count'] = test['comment_text'].apply(lambda x: check_pos_tag(x, 'verb'))
test['adj_count'] = test['comment_text'].apply(lambda x: check_pos_tag(x, 'adj'))
test['adv_count'] = test['comment_text'].apply(lambda x: check_pos_tag(x, 'adv'))
test['pron_count'] = test['comment_text'].apply(lambda x: check_pos_tag(x, 'pron'))
test['noun_density'] = test['noun_count'] / (test['word_count']+1)
test['verb_density'] = test['verb_count'] / (test['word_count']+1)
test['adj_density'] = test['adj_count'] / (test['word_count']+1)
test['adv_density'] = test['adv_count'] / (test['word_count']+1)
test['pron_density'] = test['pron_count'] / (test['word_count']+1)


# In[28]:


test.head()


# In[29]:


#get nlp features from test dataset
test_nlp_features = test.loc[:, ['word_density','upper_case_density','punctuation_density',
                                   'noun_density','verb_density','adj_density','adv_density',
                                   'pron_density']]


# ## 2.3 Sentiment Analysis Features
# Analysis using NLTK Vader SentimentAnalyser
# NLTK comes with an inbuilt sentiment analyser module – nltk.sentiment.vader—that can analyse a piece of text and classify the sentences under positive, negative and neutral polarity of sentiments. A code snippet of how this could be done is shown below:
# https://opensourceforu.com/2016/12/analysing-sentiments-nltk/

# In[30]:


import nltk
from nltk.sentiment.vader import SentimentIntensityAnalyzer
#create a sentiment intensity analyzer, 
#which return the values on three polarities: positive, negtive and neutral according to the input   
analyzer = SentimentIntensityAnalyzer()
def sentimentAnalyzer(x):
    result = analyzer.polarity_scores(x)
    return result


# In[31]:


sentimentAnalyzer('You Are Awesome')


# In[32]:


get_ipython().run_cell_magic('time', '', "train['neg'] = train['comment_text'].apply(lambda x : analyzer.polarity_scores(x)['neg'])\ntrain['neu'] = train['comment_text'].apply(lambda x : analyzer.polarity_scores(x)['neu'])\ntrain['pos'] = train['comment_text'].apply(lambda x : analyzer.polarity_scores(x)['pos'])")


# In[33]:


get_ipython().run_cell_magic('time', '', "test['neg'] = test['comment_text'].apply(lambda x : analyzer.polarity_scores(x)['neg'])\ntest['neu'] = test['comment_text'].apply(lambda x : analyzer.polarity_scores(x)['neu'])\ntest['pos'] = test['comment_text'].apply(lambda x : analyzer.polarity_scores(x)['pos'])")


# In[34]:


#get sentiment features from train and test data set
train_sentiment_features = train.loc[:, ['neg','neu','pos']]
test_sentiment_features = test.loc[:, ['neg','neu','pos']]


# ## Model Training with LogisticRegression
# We will now consider each of the feature engineering methods mentioned above from 2.1 to 2.3 .
# 
# We first test the features using LogisticRegression with a default setting. We build a classifier for each of the columns of toxicity.

# In[35]:


def train_model(classifier, feature_vector_train, feature_vector_test, class_name):
    # fit the training dataset on the classifier
    train_label = train[class_name]
    test_label = test[class_name]
    
    classifier.fit(feature_vector_train, train_label)
    # predict the labels on test dataset
    predictions_pro = classifier.predict_proba(feature_vector_test)[:,1]
    #get the roc_auc_score
    print('class name: ',class_name, '| roc_auc_score', roc_auc_score(test_label, predictions_pro))


#     class_names : ['toxic', 'severe_toxic', 'obscene', 'threat', 'insult', 'identity_hate']
#     train_word_count_features
#     train_word_tfidf_features
#     train_ngram_tfidf_features
#     train_nlp_features
#     train_sentiment_features

# In[36]:


classifier = LogisticRegression(C=0.1, solver='sag')


# In[37]:


# Logisitic regression + word TFIDF
for class_name in class_names:
    train_model(classifier, train_word_tfidf_features, test_word_tfidf_features, class_name)


# In[38]:


# Logisitic regression + n grams TF-IDF
for class_name in class_names:
    train_model(classifier, train_ngram_tfidf_features, test_ngram_tfidf_features, class_name)


# In[39]:


# Logistic regression + NLP features
for class_name in class_names:
    train_model(classifier, train_nlp_features.values, test_nlp_features.values, class_name)


# In[40]:


# Logistic regression + Sentiment analysis features
for class_name in class_names:
    train_model(classifier, train_sentiment_features.values, test_sentiment_features.values, class_name)


# In[41]:


#combine features of ngram and word TFIDF
train_word_ngram_features = hstack([train_word_tfidf_features, train_ngram_tfidf_features])
test_word_ngram_features = hstack([test_word_tfidf_features, test_ngram_tfidf_features])


# In[42]:


# try with a combined feature model
for class_name in class_names:
    train_model(classifier, train_word_ngram_features, test_word_ngram_features, class_name)


# In[43]:


#combine features with TF-IDF and sentiment features
train_word_sentiment_features = hstack([train_word_tfidf_features, train_sentiment_features.values])
test_word_sentiment_features = hstack([test_word_tfidf_features, test_sentiment_features.values])


# In[44]:


for class_name in class_names:
    train_model(classifier, train_word_sentiment_features, test_word_sentiment_features, class_name)


# In[45]:


#combine features
train_nlp_sentiment_features = np.concatenate([train_nlp_features.values, train_sentiment_features.values], axis=1)
test_nlp_sentiment_features = np.concatenate([test_nlp_features.values, test_sentiment_features.values], axis=1)


# In[46]:


for class_name in class_names:
    train_model(classifier, train_nlp_sentiment_features, test_nlp_sentiment_features, class_name)


# In[47]:


#combine features
train_word_nlp_features = hstack([train_word_tfidf_features, train_nlp_features.values])
test_word_nlp_features = hstack([test_word_tfidf_features, test_nlp_features.values])


# In[48]:


for class_name in class_names:
    train_model(classifier, train_word_nlp_features, test_word_nlp_features, class_name)


# <img src = 'features_engineering.png'>

# # Data Modeling 
# 
# We also use the word_tfidf features on other two classification algorithm, they are SVM and KNN

# ## 3.1 SVM

# In[49]:


svm_class = svm.SVC(kernel='rbf', gamma=0.7, C=1.0, probability = True)


# In[50]:


# SVM with TFIDF features
for class_name in class_names:
    train_model(svm_class, train_word_tfidf_features, test_word_tfidf_features, class_name)


# In[51]:


#Calculating the ROC AUC score 
class_name = 'threat'
feature_vector_train = train_nlp_features.values
feature_vector_test = test_nlp_features.values

train_label = train[class_name]
test_label = test[class_name]

classifier = LogisticRegression(C=0.1, solver='sag')
classifier.fit(feature_vector_train, train_label)

# predict the labels on validation dataset
predictions_bin = classifier.predict(feature_vector_test)
predictions_pro = classifier.predict_proba(feature_vector_test)[:,1]

print('roc_auc_score', roc_auc_score(test_label, predictions_pro))


# ## 3.2 KNN
# We have commented the below section of the code to avoid any issues due to heavy computation. The dataset is too large for a k nearest neighbor application.

# In[52]:



# %%time
# corpus = train['comment_text']
# vectorizer = TfidfVectorizer(stop_words='english',analyzer='word')
# X = vectorizer.fit_transform(corpus)
# knndict={}

# Y = train.drop(["comment_text","id"],axis=1)

# for item in label_cols:
    
#     y = Y[item]
#     #print(y.head())
    
#     X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.4, random_state=42, stratify=y)

#     # Create a k nearest Neighbors classifier reg
#     knn =  KNeighborsClassifier(n_neighbors=5)

#     # Fit the classifier to the training data
#     knn.fit(X_train,y_train)

#     # Print the accuracy
#     print("Score for "+item+" comments : "+str(knn.score(X_test, y_test)))
#     knndict[item]= str(knn.score(X_test, y_test)*100)[:5]+"%"

